# Auto Milestone Complete

## Application Overview

* Description:
    * Application designed to search for loans that match a criteria set in a custom field. If criteria is found then use the info from that field to push the loan to the desired milestone.
    * **Warning this will push the milestone regardless of any business rules that may be in place**

* Git Repo:
    * [Bitbucket Repo](https://bitbucket.org/jsimshighlands/auto_milestone/src/master/)
    * [Admin - Automation - HRM](https://bitbucket.org/jsimshighlands/admin-automation-hrm-form/src/master/)

* Dependent Fields:
    * CX.AUTO.MILESTONE
        * Used as the field to hold the criteria (see how it work below).
    * CX.AUTO.MILESTONE.LOG
        * Used to keep a log of when the application updates a loan.

* Dependencies:
    * SDK on PC that will be running the Windows Task Scheduler

* Optional Forms:
    * Admin - Automation - HRM
        * This form provides access to the [CX.AUTO.MILESTONE] and and a way to test if your criteria is valid or not.

## How it works

The application is schedule using Task Scheduler to run on a set interval. When the application is run it preforms a query against all loans that the Encompass user setup in the auto_milestone.exe.config file has access to and looks for field [CX.AUTO.MILESTONE] to start with roll. When if finds a loan where the [CX.AUTO.MILESTONE] field matches that criteria it adds the loan to a GUID list. If the GUID list is not empty after the query is run, it checks to see if each loan is currently locked, if it is then it moves on to the next loan and will try again on next run. If the loan is not locked it will lock the loan, split out the string that is in [CX.AUTO.MILESTONE] into arguments. If all arguments are valid then it will assign the user to the role that was part of the arguments and then push the milestone that is part of the arguments. It will then save that loan and go to the next until all GUIDs in the list have been processed.

### Criteria

* Valid criteria for [CX.AUTO.MILESTONE]:
    * roll;_role_;_user ID_;_milestone name_

* Break Down:
    * All arguments must be separated by a ; otherwise the application will not treat it as a new argument.
    * roll: If [CX.AUTO.MILESTONE] does not start with roll then it will not be picked up when the application performs its query.
    * _role_: This must be a valid 2 character role that exist in Encompass. A list of roles with their abbreviation can be found in Encompass -> Settings -> Company/User Setup -> Roles
    * _user ID_: This must be a valid user id for an existing user in Encompass. This can be found at the top of the user details screen or in the User ID column when looking at the Encompass -> Settings -> Company/User Setup -> Organization/Users
    * _milestone name_: This must be a valid milestone name that part of the loans current milestone template. This can be found by going to Encompass -> Settings -> Company/User Setup -> Milestones -> Double click on a Milestone -> Milestone Name

If unsure of the correct data to put into [CX.AUTO.MILESTONE] please either use the custom form to see if the string being used is valid or check the Auto_MS.log file found in the same folder the application runs from for any errors.

### Admin - Automation - HRM - optional

This custom form gives a testing area for potential criteria, along with being the form where you can check the criteria assigned to [CX.AUTO.MILESTONE] and view the [CX.AUTO.MILESTONE.LOG] field for any changes that have already been done to the loan.

![Form](img/full_form.png)

#### CX.AUTO.MILESTONE Trigger

This is the trigger field that criteria can be placed in by business rules to be picked up on the applications next run. If this field starts with "roll" then the application will pickup this loan and try to roll the milestone and assign the user to the role given in the criteria. Once the application runs this field will return to being an empty string and a log of that run will be placed in the CX.AUTO.MILESTONE.LOG field.

#### CX.AUTO.MILESTONE Criteria

This textbox is not mapped to any field and is simply used for testing potential criteria strings. Place a string starting with roll into the textbox and click the "Test Criteria" button. If everything checks out a message will appear stating everything is in order, otherwise a message will pop-up stating where the criteria failed.

#### CX.AUTO.MILESTONE.LOG

This field is set every time the application runs and successfully updates the loan. The timezone in the field is the zone in which the server running the application is stored. The criteria used by the application when run is next, provide a record of what role was assigned to who and milestone finished.

## Install

After download the source, make sure to change the setting in the app.config file between the <appSettings> tags to the correct Encompass server, username, and password, before you compile the first time. Once complied it is best practice to open the new auto_milestone.exe.config and copy your new encrypted <appSettings> to your app.config so that the username and password are not stored in plain text, if encryption is completed correctly then on the command prompt you will ge the message: "Config file is now encrypted".

If you are moving the compiled software to a new machine make sure copy and paste a fresh copy of the <appSettings> and type in the correct information into the auto_milestone.exe.config and then run the application from a command prompt to re-encrypt the settings as they are machine specific. If a DataProtect error is showing up in the log file try this.

### Setting up Windows Task Scheduler

1. Open Task Scheduler - this can either be found under Control Panel or by open and searching for it from the start menu.
1. Once open click on "Task Scheduler Library" - May be different depending on version of OS
1. Create a new task:
    * Name: Something descriptive
    * Description: Optional
    * Security options: Run whether user is logged on or not
        * This may prompt you to put user information when trying to finish creating the task, make sure to use the same username and password used to run the application after updating the <appSettings>
        ![Create Task General Tab](img/create_task_general.png)
    * Trigger: How often the application should run
        * Advanced Settings: Setup how often the task should repeat. Usually the application should be run every 30 minutes or at the top of the hour.
        ![Create Task Trigger](img/create_task_trigger.png)
    * Action: Start a program
        * Settings: Set the Program/Scripts to Auto_Milestone.exe
        ![Create Task Action](img/create_task_action.png)
    * Conditions and Settings Tab: These usually are good by default but can be adjusted as needed
1. Run the task manual and verify it is working as expected. Check the Task Log or the Auto_MS.log as needed to troubleshoot.

## How to use

Simple create a business rule that populates field [CX.AUTO.MILESTONE] with the correct criteria.

**WARNING** When this application rolls a milestone it does so by ignoring all other business rules that may be in place to stop the milestone from being completed. It is recommended that you only populate [CX.AUTO.MILESTONE] after running through all scenarios and making sure that all other business rules have been satisfied before populating this field.

## **Risks**

1. This application will roll the milestone that is part of the criteria. This means:
    1. If you roll multiple milestones only the role that is also part of the criteria will be populated
    1. Users may not have access to fields in the new milestone after being rolled to go back and fill in if a field is required for an earlier milestone
    1. Users may not have access to the loan if a role is not filled in
1. This application will assign users to any role even if that user does not have the persona that matches that role.

## Best Practices

1. Write a business rule that checks or have a business process that verifies that all fields and documents required to roll the milestone have been completed before populating [CX.AUTO.MILESTONE].
1. Only ever roll one milestone ahead. If the loan is currently in File Started, only roll to Processing if possible.
1. If possible only assign que users to the a role.
1. When setting up the Task Scheduler make sure to use the same users that encrypted the auto_milestone.exe.config file as the user that the task runs under.

## Troubleshooting

1. If a loan has not rolled that is expected to, first check that field [CX.AUTO.MILESTONE] has been populated with the correct criteria. If it is then check the log file found in the same directory as the Auto_Milestone.exe to see what may be erring out. The log will capturer all system exceptions that thrown along with some information if a user, role or milestone are not typed correctly.
1. The auto_milestone.exe.config file is an XML file, any username and passwords using special characters as defined by XML must use the escape character for that character for the value to be passed to the application. For example an & must become &amp; to be passed correctly to the application.