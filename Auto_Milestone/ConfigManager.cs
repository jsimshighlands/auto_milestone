﻿using System;
using System.Configuration;

namespace Auto_Milestone
{
    class ConfigManager
    {
        public Configuration ConfigFile { get; }
        public string ProtectionProvider { get; set; }
        public string ConfigSectionName { get; set; }
        public ConfigurationSection ConfigSection { get; set; }

        public ConfigManager(Configuration configFile, string protectionProvider, string configSectionName)
        {
            this.ConfigFile = configFile;
            this.ProtectionProvider = protectionProvider;
            this.ConfigSectionName = configSectionName;
            this.ConfigSection = ConfigFile.GetSection(configSectionName);
        }

        public void ConfigProtect()
        {
            if (!ConfigSection.SectionInformation.IsProtected)
            {
                ConfigSection.SectionInformation.ProtectSection(ProtectionProvider);
                ConfigSection.SectionInformation.ForceSave = true;
                ConfigFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(ConfigSectionName);
                log.Info("Config file was encrypted");
                Console.WriteLine("Config file is now encrypted");
            }
            else
            {
                log.Info("Config file was already encrypted");
                Console.WriteLine("Config file was already encrypted");
            }
        }

        public void ConfigUnprotect()
        {
            if (ConfigSection.SectionInformation.IsProtected)
            {
                ConfigSection.SectionInformation.UnprotectSection();
                ConfigurationManager.RefreshSection(ConfigSectionName);
                log.Info("Config File was unencrypted");
                Console.WriteLine("Config file unencrypted");
            }
            else
            {
                log.Info("Config was already unencrypted");
                Console.WriteLine("Config file is already unencrypted");
            }
        }

        public bool IsProtected()
        {
            if (ConfigSection.SectionInformation.IsProtected)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }
}
