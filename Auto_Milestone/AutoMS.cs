﻿using System;
using System.Collections.Generic;
using System.Configuration;
using EllieMae.Encompass.Runtime;
using EllieMae.Encompass.Client;
using EllieMae.Encompass.Query;
using EllieMae.Encompass.BusinessObjects.Users;
using EllieMae.Encompass.BusinessObjects.Loans;
using EllieMae.Encompass.BusinessObjects.Loans.Logging;


namespace Auto_Milestone
{
    class AutoMS
    {
        static void Main(string[] args)
        {
            log.Info("=============== Application Starting ===============");

            ConfigManager myConfig = new ConfigManager(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None), "DataProtectionConfigurationProvider", "appSettings");
            if (!myConfig.IsProtected())
            {
                myConfig.ConfigProtect();
                log.Info("================== Task Completed ==================");
                return;
            }

            myConfig.ConfigUnprotect();

            new RuntimeServices().Initialize();

            startApp();
            log.Info("================== Task Completed ==================");
        }

        private static void startApp()
        {
            try
            {
                String encPass = ConfigurationManager.AppSettings["encPassword"];
                String encEnv = ConfigurationManager.AppSettings["encServer"];
                String encUser = ConfigurationManager.AppSettings["encUsername"];

                Session s = new Session();
                s.Start(encEnv, encUser, encPass);

                log.Info($"Logged into Client ID: {s.ClientID}");

                List<string> loanGUIDs = findLoans("CX.AUTO.MILESTONE", "roll", s);

                if (loanGUIDs.Count > 0)
                {
                    log.Info($"Found {loanGUIDs.Count.ToString()} loans that will be rolled to a new milestone.");
                    foreach (var loanGUID in loanGUIDs)
                    {
                        using (var loan = s.Loans.Open(loanGUID, false, false))
                        {
                            // Get loans current lock state
                            var isLocked = loan.GetCurrentLock();

                            // If loan is not locked lock it and make changes
                            if (isLocked == null)
                            {
                                log.Info($"Updating loan {loanGUID.ToString()}");
                                //Console.WriteLine("Loan is not locked");
                                loan.Lock();

                                //Get Role, User, and milestone from loan
                                string rollInfo = loan.Fields["CX.AUTO.MILESTONE"].Value.ToString();
                                log.Info($"--Values used to roll the milestone for loan {loan.Fields["364"].Value.ToString()} are: {rollInfo}");
                                var msInfo = rollInfo.Split(';');

                                if (msInfo.Length < 4)
                                {
                                    log.Warn($"--Not enough arguments in roll value, please check the value in field [CX.AUTO.MILESTONE]");
                                    continue;
                                }

                                log.Info($"--Getting Role: {msInfo[1]}");
                                Role assignRole = s.Loans.Roles.GetRoleByAbbrev(msInfo[1]);

                                if (assignRole == null)
                                {
                                    log.Error($"--No role {msInfo[1]} found.");
                                    continue;
                                }

                                log.Info($"--Assign Role: {assignRole.Name}");
                                log.Info($"--Getting User: {msInfo[2]}");

                                User assignUser = s.Users.GetUser(msInfo[2]);

                                if (assignUser == null)
                                {
                                    log.Error($"--No user {msInfo[2]} found.");
                                    continue;
                                }

                                log.Info($"--Assign User: {assignUser.FullName} ({assignUser.ID})");
                                
                                loan.Associates.AssignUser(assignRole, assignUser);
                                log.Info("--User assigned to role");

                                log.Info($"--Getting Milestone: {msInfo[3]}");
                                MilestoneEvent ms = loan.Log.MilestoneEvents.GetEventForMilestone(msInfo[3]);

                                if (ms == null)
                                {
                                    log.Error($"--No Milestone {msInfo[3]} found.");
                                    continue;
                                }

                                ms.Completed = true;
                                ms.AdjustDate(DateTime.Now, false, true);
                                log.Info($"--{ms.MilestoneName} Milestone completed");

                                loan.Fields["CX.AUTO.MILESTONE"].Value = String.Empty;
                                log.Info("--Reset field [CX.AUTO.MILESTONE] to blank");

                                string autoMSLog = DateTime.Now.ToString() + "\t" + rollInfo;

                                if ( loan.Fields["CX.AUTO.MILESTONE.LOG"].Value.ToString() != String.Empty)
                                {
                                    loan.Fields["CX.AUTO.MILESTONE.LOG"].Value = loan.Fields["CX.AUTO.MILESTONE.LOG"].Value.ToString() + "\n" + autoMSLog;
                                }
                                else
                                {
                                    loan.Fields["CX.AUTO.MILESTONE.LOG"].Value = autoMSLog;
                                }

                                loan.Commit();
                                log.Info($"--Loan {loan.Fields["364"].Value.ToString()} has been pushed to the {ms.MilestoneName} milestone.");
                            }

                            //Loan is locked log it and it will be picked on next run.
                            else
                            {
                                log.Warn($"Loan {loan.Fields["364"].Value.ToString()} is locked by: {isLocked.LockedBy}, since: {isLocked.LockedSince} PST. Will try on next run.");
                            }
                        }
                    }
                }
                else
                {
                    log.Info("No loans found");
                }

                s.End();
                log.Info("Closed session with Encompass Server");
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
                Console.WriteLine("Exception thrown, check log file");
            }
        }

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static List<string> findLoans(string customField, string searchValue, Session activeS)
        {
            try
            {
                //Returns a list of loans that match the search criteria, note that this critera is starts with
                StringFieldCriterion strCrit = new StringFieldCriterion();
                strCrit.FieldName = "Fields." + customField;
                strCrit.Value = searchValue.ToLower();
                strCrit.MatchType = StringFieldMatchType.StartsWith;

                log.Info("Query Started.");
                var foundLoans = activeS.Loans.Query(strCrit);
                var loanGUIDs = new List<string>();

                for (int i = 0; i < foundLoans.Count; i++)
                {
                    loanGUIDs.Add(foundLoans[i].Guid.ToString());
                }

                log.Info("Query Completed.");
                return loanGUIDs;
            }
            catch (Exception e)
            {
                log.Error(e.ToString());
                Console.WriteLine("Exception thrown, check log file");
                List<string> emptyList = new List<string>();
                return emptyList;
            }
        }
    }
}
